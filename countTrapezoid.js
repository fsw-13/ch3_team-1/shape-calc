function countTrapezoid(a, b, c) {
    return 0.5 * (Number(a) + Number(b)) * c;
}

module.exports = {
    countTrapezoid
}